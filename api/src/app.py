import numpy as np
import pandas as pd
import ast
import os

# all lightfm imports
from lightfm.data import Dataset
from lightfm import LightFM
from lightfm import cross_validation
from lightfm.evaluation import precision_at_k
from lightfm.evaluation import auc_score

# imports re for text cleaning
import re
from datetime import datetime, timedelta

# we will ignore pandas warning
import warnings
warnings.filterwarnings('ignore')

from flask import Flask, request

app = Flask(__name__)
port = int(os.environ.get('PORT', 7000))

import pickle

def recommend_questions(user_ids):
    """
    Returns recommendations for a user,
    where each recommendation has a weight_loss, eating_healthy, muscle_gain scores.
    Number of recommendations is capped at 8
    """
    for user in user_ids:
        # print their previous viewed recipe title
        previous_q_id_num = df_interact_infer.loc[df_interact_infer['user_id_num'] == user][:3]['recipe_id_num']
        df_previous_questions = df_recipes_clean_infer.loc[df_recipes_clean_infer['recipe_id_num'].isin(previous_q_id_num)]
        print('User Id (' + str(user) + "): Previous Viewed Recipes")

        # predict
        discard_qu_id = df_previous_questions['recipe_id_num'].values.tolist()
        df_use_for_prediction = df_recipes_clean_infer.loc[~df_recipes_clean_infer['recipe_id_num'].isin(discard_qu_id)]
        questions_id_for_predict = df_use_for_prediction['recipe_id_num'].values.tolist()

        scores = model.predict(
            user,
            questions_id_for_predict,
            item_features=recipe_features_infer,
            user_features=user_features_infer)

        df_use_for_prediction['scores'] = scores
        df_use_for_prediction = df_use_for_prediction.sort_values(by='scores', ascending=False)[:8]
        print('User Id (' + str(user) + "): Recommended Recipes: ")
        print(df_use_for_prediction[['recipename', 'recipe_features', 'eh_score', 'wl_score', 'mg_score', 'scores']])
        json_output = df_use_for_prediction[['recipename', 'recipe_features', 'eh_score', 'wl_score', 'mg_score', 'scores']].to_json()
        print('\nJson Output for Inference: \n', json_output)
        return json_output

@app.route("/predict", methods=["POST"])
def predict():
    if request.method == "POST":
        user_id = request.form.get('user_id')
        input_user_id = [user_id]
        return recommend_questions(input_user_id)

if __name__ == "__main__":
    # Load features and models
    model = pickle.load(open("./api/models/model.pickle","rb"))
    df_interact_infer = pickle.load(open("./api/models/df_interact_infer.pickle","rb"))
    df_recipes_clean_infer = pickle.load(open("./api/models/df_recipes_clean_infer.pickle","rb"))
    df_user_viewed_recipe_infer = pickle.load(open("./api/models/df_user_viewed_recipe_infer.pickle","rb"))
    recipe_features_infer = pickle.load(open("./api/models/recipe_features_infer.pickle","rb"))
    user_features_infer = pickle.load(open("./api/models/user_features_infer.pickle","rb"))
    app.run(host='0.0.0.0', port=port)
