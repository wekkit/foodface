import numpy as np
import pandas as pd
import ast
import os

# all lightfm imports 
from lightfm.data import Dataset
from lightfm import LightFM
from lightfm import cross_validation
from lightfm.evaluation import precision_at_k
from lightfm.evaluation import auc_score

# imports re for text cleaning 
import re
from datetime import datetime, timedelta

# we will ignore pandas warning 
import warnings
warnings.filterwarnings('ignore')

from flask import Flask, request

app = Flask(__name__)
port = int(os.environ.get('PORT', 7000))

import pickle

def display_side_by_side(*args):
    html_str=''
    for df in args:
        html_str+=df.to_html()
    display_html(html_str.replace('table','table style="display:inline"'),raw=True)


def recommend_questions(professional_ids):
     
    for professional in professional_ids:
        # print their previous answered question title
        previous_q_id_num = df_merge.loc[df_merge['user_id_num'] == professional][:3]['recipe_id_num']
        df_previous_questions = df_questions.loc[df_questions['recipe_id_num'].isin(previous_q_id_num)]
        print('Professional Id (' + str(professional) + "): Previous Answered Questions")
#         display_side_by_side(
#             df_previous_questions[['recipename', 'recipe_features']],
#             df_professionals.loc[df_professionals.user_id_num == professional][['user_id_num','dietary_list_tags']])
        
        # predict
        discard_qu_id = df_previous_questions['recipe_id_num'].values.tolist()
        df_use_for_prediction = df_questions.loc[~df_questions['recipe_id_num'].isin(discard_qu_id)]
        questions_id_for_predict = df_use_for_prediction['recipe_id_num'].values.tolist()
        
        scores = model.predict(
            professional,
            questions_id_for_predict,
            item_features=questions_features,
            user_features=professional_features)
        
        df_use_for_prediction['scores'] = scores
        df_use_for_prediction = df_use_for_prediction.sort_values(by='scores', ascending=False)[:8]
        print('Professional Id (' + str(professional) + "): Recommended Questions: ")
        print(df_use_for_prediction[['recipename', 'recipe_features', 'eh_score', 'wl_score', 'mg_score', 'scores']])
        json_output = df_use_for_prediction[['recipename', 'recipe_features', 'eh_score', 'wl_score', 'mg_score', 'scores']].to_json()
#         display(df_use_for_prediction[['recipename', 'recipe_features', 'eh_score', 'wl_score', 'mg_score', 'scores']])
        print(json_output)
        return json_output

@app.route("/predict", methods=["POST"])
def predict():
    if request.method == "POST":
        user_id = request.form.get('user_id')
        input_user_id = [user_id]
        return recommend_questions(input_user_id)

if __name__ == "__main__":
    model = pickle.load(open("./models/model.pickle","rb"))
    df_merge = pickle.load(open("./models/df_merge.pickle","rb"))
    df_questions = pickle.load(open("./models/df_questions.pickle","rb"))
    df_professionals = pickle.load(open("./models/df_professionals.pickle","rb"))
    questions_features = pickle.load(open("./models/questions_features.pickle","rb"))
    professional_features = pickle.load(open("./models/professional_features.pickle","rb"))
    app.run(host='0.0.0.0', port=port)
