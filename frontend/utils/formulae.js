export function calculateEatingHealthy(args) {
  const factors = Object.assign({}, args)
  for (let key in factors) {
    if (factors[key] !== 0 && !Boolean(factors[key])) {
      console.warn(`factor not found: ${key}`)
      return '-'
    }
    if (key !== 'serving') factors[key]++
  }
  const { tFat, carbs, protein, fibre, serving } = factors
  const calculation = (protein * fibre) / (tFat * carbs) / serving
  return calculation * 100
}

export function calculateWeightLoss(args) {
  const factors = Object.assign({}, args)

  for (let key in factors) {
    if (!Boolean(factors[key])) {
      console.warn(`factor not found: ${key}`)
      return '-'
    }
    if (key !== 'serving') factors[key]++
  }
  const { tFat, carbs, protein, fibre, cal, serving } = factors
  const calculation = (protein * fibre) / (cal * cal * tFat * carbs) / serving
  return calculation * 10000000
}

export function calculateMuscleGain(args) {
  const factors = Object.assign({}, args)
  for (let key in factors) {
    if (!Boolean(factors[key])) {
      console.warn(`factor not found: ${key}`)
      return '-'
    }
    if (key !== 'serving') factors[key]++
  }
  const { tFat, carbs, protein, fibre, cal, serving } = factors
  const calculation =
    (protein * protein * fibre * carbs) / (tFat * cal) / serving
  return calculation * 100
}
