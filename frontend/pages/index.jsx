import React, { useState } from 'react'
import { makeStyles } from '@material-ui/styles'
import sampleJson from '../sample.json'
import { Button, TextField } from '@material-ui/core'

const useStyles = makeStyles({
  body: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    maxWidth: 1000,
    marginTop: 24,
    margin: 'auto'
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 24
  },
  textInput: {
    width: 200,
    marginRight: 20
  },
  userInput: {
    display: 'flex',
    flex: 0
  },
  sortSelection: {
    display: 'flex',
    justifyContent: 'space-around'
  },
  row: {
    display: 'flex',
    flexDirection: 'row'
  },
  listRow: {
    color: 'white',
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#1e88e5',
    borderRadius: 12,
    padding: 18,
    paddingTop: 24,
    paddingBottom: 24,
    marginTop: 12,
    boxShadow: '3px 3px 3px #ccc'
  },
  item: {
    flex: 1,
    textAlign: 'center'
  },
  itemName: {
    flex: 3
  },
  btn: {
    margin: 10
  }
})

export default function App() {
  const [selected, setSelected] = useState('eh')
  const [id, setId] = useState('')
  const classes = useStyles()

  const convertJson = json => {
    const recipes = []
    for (let key in json.recipename) {
      recipes.push({
        name: json.recipename[key],
        eh: json.eh_score[key],
        wl: json.wl_score[key] * 1000000,
        mg: json.mg_score[key]
      })
    }
    return recipes.filter((x, i) => i < 5)
  }
  const [recipes, setRecipes] = useState([])

  const fetchData = async name => {
    if (!name) return
    try {
      const newRecipes = convertJson(sampleJson)
      function shuffle(array) {
        var currentIndex = array.length,
          temporaryValue,
          randomIndex
        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex)
          currentIndex -= 1
          temporaryValue = array[currentIndex]
          array[currentIndex] = array[randomIndex]
          array[randomIndex] = temporaryValue
        }
        return array
      }

      const shuffled = shuffle(newRecipes)
      setRecipes(shuffled)
    } catch (e) {
      console.log(e)
      return
    }
  }

  return (
    <div className={classes.body}>
      <div className={classes.header}>
        <div className={classes.userInput}>
          <TextField
            className={classes.textInput}
            onChange={({ target: { value } }) => setId(value)}
            value={id}
            id="standard-basic"
            label="User ID"
          />
          <Button variant="contained" onClick={() => fetchData(id)}>
            Recommend
          </Button>
        </div>
        <div className={classes.sortSelection}>
          <Button
            className={classes.btn}
            variant={selected === 'eh' ? 'contained' : null}
            onClick={() => {
              setSelected('eh')
              setRecipes(prev =>
                [...prev].sort((a, b) => (a.eh < b.eh ? 1 : -1))
              )
            }}
          >
            Eating healthy
          </Button>
          <Button
            className={classes.btn}
            variant={selected === 'wl' ? 'contained' : null}
            onClick={() => {
              setSelected('wl')
              setRecipes(prev =>
                [...prev].sort((a, b) => (a.wl < b.wl ? 1 : -1))
              )
            }}
          >
            Weight loss
          </Button>
          <Button
            className={classes.btn}
            variant={selected === 'mg' ? 'contained' : null}
            onClick={() => {
              setSelected('mg')
              setRecipes(prev =>
                [...prev].sort((a, b) => (a.mg < b.mg ? 1 : -1))
              )
            }}
          >
            Muscle gain
          </Button>
        </div>
      </div>
      <div className={classes.row}>
        <div className={classes.itemName} />
        <h4 className={classes.item}>Eating healthy</h4>
        <h4 className={classes.item}>Weight loss</h4>
        <h4 className={classes.item}>Muscle gain</h4>
      </div>
      {recipes.map((item, i) => (
        <div className={classes.listRow} key={item.name + i}>
          <div className={classes.itemName}>
            {i + 1}. {item.name}
          </div>
          <div className={classes.item}>{(item.eh || 0).toFixed(2)}</div>
          <div className={classes.item}>{(item.wl || 0).toFixed(2)}</div>
          <div className={classes.item}>{(item.mg || 0).toFixed(2)}</div>
        </div>
      ))}
    </div>
  )
}
