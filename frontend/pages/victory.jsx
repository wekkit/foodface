import React, { useEffect, useState } from 'react'
import {
  VictoryBar,
  VictoryScatter,
  VictoryChart,
  VictoryAxis,
  VictoryTheme,
  VictoryArea,
  VictoryPie
} from 'victory'

const useData = (interval = 3000) => {
  const [data, setData] = useState([
    { quarter: 1, earnings: Math.random() * 1000 },
    { quarter: 2, earnings: Math.random() * 1000 },
    { quarter: 3, earnings: Math.random() * 1000 },
    { quarter: 4, earnings: Math.random() * 1000 }
  ])
  useEffect(() => {
    setInterval(() => {
      setData([
        { quarter: 1, earnings: Math.random() * 1000 },
        { quarter: 2, earnings: Math.random() * 1000 },
        { quarter: 3, earnings: Math.random() * 1000 },
        { quarter: 4, earnings: Math.random() * 1000 }
      ])
    }, interval)
  }, [])
  return data
}

export default function App() {
  const barData = useData(3000)
  const scatterData = useData(3800)
  const areaData = useData(4432)
  const pieData = useData(6232)

  return (
    <div className="body">
      <div className="container">
        <VictoryChart
          theme={VictoryTheme.material}
          animate={{ duration: 1000, easing: 'quad' }}
          domainPadding={20}
        >
          <VictoryAxis
            tickValues={[1, 2, 3, 4]}
            tickFormat={['Q1', 'Q2', 'Q3', 'Q4']}
          />
          <VictoryAxis dependentAxis tickFormat={x => `${x / 1000}k`} />
          <VictoryBar data={barData} x="quarter" y="earnings" />
        </VictoryChart>
      </div>
      <div className="container">
        <VictoryChart
          theme={VictoryTheme.material}
          animate={{ duration: 1000, easing: 'quad' }}
          domainPadding={20}
        >
          <VictoryAxis
            tickValues={[1, 2, 3, 4]}
            tickFormat={['Q1', 'Q2', 'Q3', 'Q4']}
          />
          <VictoryAxis dependentAxis tickFormat={x => `${x / 1000}k`} />
          <VictoryScatter data={scatterData} x="quarter" y="earnings" />
        </VictoryChart>
      </div>
      <div className="container">
        <VictoryChart
          theme={VictoryTheme.material}
          animate={{ duration: 1000, easing: 'quad' }}
          domainPadding={20}
        >
          <VictoryAxis
            tickValues={[1, 2, 3, 4]}
            tickFormat={['Q1', 'Q2', 'Q3', 'Q4']}
          />
          <VictoryAxis dependentAxis tickFormat={x => `${x / 1000}k`} />
          <VictoryArea data={areaData} x="quarter" y="earnings" />
        </VictoryChart>
      </div>
      <div className="container">
        <VictoryPie
          theme={VictoryTheme.material}
          labels={['1', '2', '3', '4']}
          animate={{ duration: 1000, easing: 'quad' }}
          data={pieData}
          x="quarter"
          y="earnings"
        />
      </div>

      <style jsx>{`
        .body {
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
        }
        .container {
          width: 500px;
          background-color: white;
        }
      `}</style>
    </div>
  )
}
