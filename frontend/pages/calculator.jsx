import React, { useState } from 'react'
import { TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { VictoryBar, VictoryGroup, VictoryTheme } from 'victory'
import {
  calculateEatingHealthy,
  calculateMuscleGain,
  calculateWeightLoss
} from '../utils/formulae'

const useStyles = makeStyles({
  body: {
    display: 'flex',
    flexDirection: 'row',
    margin: '24px auto',
    maxWidth: 800
  },
  calculator: { flex: 1 },
  results: {
    flex: 1,
    marginTop: 80,
    justifyContent: 'center',
    textAlign: 'center'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: '200px'
  },
  inputField: {
    marginTop: 20
  }
})

export default function Page() {
  const classes = useStyles()
  const [sFat, setSFat] = useState(1.63)
  const [tFat, setTFat] = useState(5.75)
  const [carbs, setCarbs] = useState(10.5)
  const [protein, setProtein] = useState(41)
  const [fibre, setFibre] = useState(1.4)
  const [cal, setCal] = useState(252.25)
  const [serving, setServing] = useState(4)

  const args = {
    sFat,
    tFat,
    carbs,
    protein,
    fibre,
    cal,
    serving
  }
  const score = [
    { label: 'eatingHealthy', score: calculateEatingHealthy(args) },
    { label: 'weightLoss', score: calculateWeightLoss(args) },
    { label: 'muscleGain', score: calculateMuscleGain(args) }
  ]

  return (
    <div className={classes.body}>
      <div className={classes.calculator}>
        <h1>Index calculator</h1>
        <form className={classes.form} noValidate autoComplete="off">
          <TextField
            className={classes.inputField}
            type="number"
            label="Saturated fat"
            value={sFat}
            onChange={({ target: { value } }) => setSFat(value)}
          />
          <TextField
            className={classes.inputField}
            type="number"
            label="Total fat"
            value={tFat}
            onChange={({ target: { value } }) => setTFat(value)}
          />
          <TextField
            className={classes.inputField}
            type="number"
            label="Carbs"
            value={carbs}
            onChange={({ target: { value } }) => setCarbs(value)}
          />
          <TextField
            className={classes.inputField}
            type="number"
            label="Protein"
            value={protein}
            onChange={({ target: { value } }) => setProtein(value)}
          />
          <TextField
            className={classes.inputField}
            type="number"
            label="Fibre"
            value={fibre}
            onChange={({ target: { value } }) => setFibre(value)}
          />
          <TextField
            className={classes.inputField}
            type="number"
            label="Calories"
            value={cal}
            onChange={({ target: { value } }) => setCal(value)}
          />
          <TextField
            className={classes.inputField}
            type="number"
            label="Serving"
            value={serving}
            onChange={({ target: { value } }) => setServing(value)}
          />
        </form>
      </div>
      <div className={classes.results}>
        <div>
          <h1>Eating healthy</h1>
          <p>{calculateEatingHealthy(args)}</p>
        </div>
        <div>
          <h1>Weight loss</h1>
          <p>{calculateWeightLoss(args)}</p>
        </div>
        <div>
          <h1>Muscle gain</h1>
          <p>{calculateMuscleGain(args)}</p>
        </div>

        <VictoryGroup
          theme={VictoryTheme.material}
          animate={{ duration: 1000, easing: 'quad' }}
          domainPadding={50}
        >
          <VictoryBar data={score} x="label" y="score" />
        </VictoryGroup>
      </div>
    </div>
  )
}
