const fs = require('fs')
const csv = require('csvtojson')

function calculateEatingHealthy(args) {
  const factors = Object.assign({}, args)
  for (let key in factors) {
    if (factors[key] !== 0 && !Boolean(factors[key])) {
      console.warn(`factor not found: ${key}`)
      return '-'
    }
    if (key !== 'serving') factors[key]++
  }
  const { tFat, carbs, protein, fibre, serving } = factors
  const calculation = (protein * fibre) / (tFat * carbs) / serving
  return calculation * 100
}

function calculateWeightLoss(args) {
  const factors = Object.assign({}, args)

  for (let key in factors) {
    if (!Boolean(factors[key])) {
      console.warn(`factor not found: ${key}`)
      return '-'
    }
    if (key !== 'serving') factors[key]++
  }
  const { tFat, carbs, protein, fibre, cal, serving } = factors
  const calculation = (protein * fibre) / (cal * cal * tFat * carbs) / serving
  return calculation * 10000000
}
function calculateMuscleGain(args) {
  const factors = Object.assign({}, args)
  for (let key in factors) {
    if (!Boolean(factors[key])) {
      console.warn(`factor not found: ${key}`)
      return '-'
    }
    if (key !== 'serving') factors[key]++
  }
  const { tFat, carbs, protein, fibre, cal, serving } = factors
  const calculation =
    (protein * protein * fibre * carbs) / (tFat * cal) / serving
  return calculation * 100
}

async function body() {
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index
  }

  const jsonArray = await csv().fromFile('./df_recipes_clean.csv')
  let recipes = jsonArray
    .map(item => {
      const name = item.recipename
      const tFat = item.totalfat
      const carbs = item.carbs
      const protein = item.protein
      const fibre = item.fibre
      const cal = item.calories
      const serving = item.servings
      return {
        name,
        eh: calculateEatingHealthy({
          tFat,
          carbs,
          protein,
          fibre,
          cal,
          serving
        }),
        wl: calculateWeightLoss({
          tFat,
          carbs,
          protein,
          fibre,
          cal,
          serving
        }),
        mg: calculateMuscleGain({
          tFat,
          carbs,
          protein,
          fibre,
          cal,
          serving
        })
      }
    })
    .filter(onlyUnique)

  recipes.sort((a, b) => (a.eh < b.eh ? 1 : -1))
  recipes = recipes.map((item, i) => {
    return { ...item, ehRanking: i + 1 }
  })
  recipes.sort((a, b) => (a.wl < b.wl ? 1 : -1))
  recipes = recipes.map((item, i) => {
    return { ...item, wlRanking: i + 1 }
  })
  recipes.sort((a, b) => (a.mg < b.mg ? 1 : -1))
  recipes = recipes.map((item, i) => {
    return { ...item, mgRanking: i + 1 }
  })
  fs.writeFileSync('recipes.json', JSON.stringify(recipes))
}

body()
