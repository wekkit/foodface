# Yoripe Recipe Recommender

![python](https://img.shields.io/badge/python%20-3.7.1-brightgreen.svg) ![LightFM](https://img.shields.io/badge/LightFM-1.15-orange)

## Table of contents

- [Introduction](#introduction)
  - [Quick Summary](#quick-summary)
  - [Motivation](#motivation)
- [Recommending Recipes](#recommending-recipes)
  - [Data](#data)
  - [Insights](#insights)
  - [Methodology & Results](#methodology-&-results)
    1. [Recommender Model](#recommender-model)
    2. [Health-based Ranker](#health-based-ranker)
- [Conclusion](#conclusion)
  - [Closing Thoughts](#closing-thoughts)
  - [Future Work](#future-work)
- [Quick Start](#quick-start)
  - [Requirements](#requirements)
  - [Deploy Recommender](#deploy-recommender)
- [Resources](#resources)
  - [References](#references)
- [Team](#team)
- [Awards](#awards)
- [Special Thanks](#special-thanks)

---

# Introduction

The repository is part of a 2-day hackathon submission. It includes a **web deployment of a recipe recommender model**, trained from consumer data acquired from Yoripe.

#### Quick Summary

Built a multi-layer recipe recommendation model based on users' **recipe-viewing history** and **preferred health goals**,

- Hybrid matrix fatorization recommender model
- Health-based ranker, based on 3 scoring metrics: Eating healthy, weight loss, muscle gain

#### Motivation

The problem statement and objectives are clear. We are looking into how we can better recommend recipes and meal plan for busy households, nudge people to eat healthier and discover hidden insights for retailers and FMCGs to sell and market smarter.

# Recommending Recipes

#### Data

The dataset is a collection of app data from Yoripe. We have access to user preferences, recipe information, nutrition facts, user-to-recipe viewing history, and 3GB worth of google analytics app data.

#### Insights

One of the most valuable analyses we found was user fallout rate. We defined fallout rate as the churn rate, i.e. the percentage of users who stopped using the app within a specific time period.

<img src="/img/treemap.png" class="center" width="600" height="400">

After analyzing all user-activity since the app's first release in 01 March 2018. We discovered that upon launching the app,

- 68% of users clicked on a recommended recipe in the 'Explore' page
- 25% of users exited the app after some scrolling activity (without clicking on anything)
- 7% of users scrolled through the 'Explore' page but did not click on anything, and switched to the 'Search' page

Although Yoripe did a good job in achieving a moderately-high app engagement rate, we know that a significant pool of users were unable to find the recipes they want. We speculate that the 25% fallout/churn rate was due to the following:

1. Possible decision fatigue
2. Unfamiliar recipes
3. Recipes not personalized enough

#### Methodology & Results

##### Recommender Model

After preprocessing the data, we only had about 875 valid user-recipe interactions, with ~340 unique users. and ~500 unique recipes.

Due to the lack of interaction data, we want a model that can allow recommendations to generalise to new items (via item features) and to new users (via user features).

We chose _LightFM_ because this hybrid matrix factorisation algorithm outperforms both collaborative and content-based models in cold-start or sparse interaction data scenarios (using both user and item metadata). Embeddings produced by _LightFM_ encode important semantic information about features, which are incredibly relevant for our problem. As a baseline, we chose users' dietary preferences as our user features and recipe's dietary tags as recipe features.

Our model did exceedingly well (number wise), achieving an AUC of 94.6%. But take this with a grain of salt, the data for which was evaluated on was not expected to give statistically significant results.

##### Health-based Ranker

The lack of training data led us to add a ranker on top of the recommendation model. For all recommendations generated, we impose a health-based scoring mechanism to rank them again. We have 3 health-based scoring mechanism, and all of them are carefully defined by a f&b domain expert.

1. Muscle gain: ((Protein^2Fibre)/(Total fatsCalories))/Serving
2. Weight loss: ((ProteinFibre)/(Total fatsCarbs\*Calories^2))/Serving
3. Eating Healthy: ((ProteinFibre)/(Total fatsCarbs))/Serving

The intuition is to **enable recommendations that consider users' health goals**.

# Conclusion

#### Closing Thoughts

Yoripe's recipe 'Explore' page was already getting decent engagements without personalized recommendations. However, they are still facing issues regarding user-retention and user-engagements. Food recipe/cooking apps are all over the Playstore/AppStore. With major players like Yummly, Tasty, and 下厨房, Yoripe has to further differentiate and compete with them. We feel that providing personalized recommendations is definitely a feature worth exploring as the app matures.

#### Future Work

- Develop ETL pipeline for google analytics data
- Add more user-related features when training the recommender
- Use other recommender algorithms, e.g. [Alibaba's Alink](https://github.com/alibaba/alink)

# Quick Start

#### Requirements

```
pip install -r requirements.txt
```

#### Deploy Recommender Interface

1. Navigate into the `frontend` directory: `cd frontend`
2. Run `npm install`
3. Run `npm run dev`
4. Navigate to http://localhost:3000 to view the recommendations interface.

Currently, the results are imported from the `sample.json` file in the `frontend` directory. This hardcoding was done for demo purposes - ideally the results are available via the api. This behaviour can be modified in the `fetchData` function in `pages/index.jsx` - instead of using data imported from the `sample.json` file, it should make an API call to the backend.

# Resources

This list is a non-exhaustive list of the main sources of information we used in doing this project.

#### References

- https://arxiv.org/abs/1507.08439
- http://lyst.github.io/lightfm/docs/home.html
- https://www.kaggle.com/niyamatalmass/lightfm-hybrid-recommendation-system
- https://www.tibco.com/
- https://yoripe.com/

# Team Foodface

<img src="/img/foodface.png" width="300" height="100">

Zhengsen, Traci, Keith, Jeremy, Elwood, Eugene

# Awards

1st Place-Winner of [SheLovesData/Tibco Hackathon: Hack Your Way To A Healthier Lifestyle](https://shelovesdata.com/event/shelovesdata-singapore-she-loves-data-hackathon/)

# Special Thanks

<img src="/img/shelovesdata.png" width="300" height="300">
<img src="/img/yoripe.png" width="500" height="300">
<img src="/img/tibco.png" width="300" height="300">
